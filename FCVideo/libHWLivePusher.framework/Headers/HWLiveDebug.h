//Copyright 2018 Huawei Technologies Co.,Ltd.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
/// 流状态
typedef NS_ENUM (NSUInteger, HWLiveState){
    /// 准备
    HWLiveReady = 0,
    /// 连接中
    HWLivePending = 1,
    /// 已连接
    HWLiveStart = 2,
    /// 已断开
    HWLiveStop = 3,
    /// 连接出错
    HWLiveError = 4,
    ///  正在刷新
    HWLiveRefresh = 5
};

typedef NS_ENUM (NSUInteger, HWLiveSocketErrorCode) {
    HWLiveSocketError_PreView = 201,              ///< 预览失败
    HWLiveSocketError_GetStreamInfo = 202,        ///< 获取流媒体信息失败
    HWLiveSocketError_ConnectSocket = 203,        ///< 连接socket失败
    HWLiveSocketError_Verification = 204,         ///< 验证服务器失败
    HWLiveSocketError_ReConnectTimeOut = 205      ///< 重新连接服务器超时
};

@interface HWLiveDebug : NSObject

//@property (nonatomic, copy) NSString *streamId;                       ///< 流id
@property (nonatomic, copy) NSString *uploadUrl;                        ///< 流地址
@property (nonatomic, assign) CGSize videoSize;                         ///< 上传的分辨率
@property (nonatomic, assign) BOOL isRtmp;                              ///< 上传方式（现在只支持rtmp方式）

@property (nonatomic, assign) CGFloat elapsedMilli;                     ///< 距离上次统计的时间 单位ms
@property (nonatomic, assign) long long timeStamp;                      ///< 当前的时间戳，从而计算1s内数据
@property (nonatomic, assign) CGFloat dataFlow;                         ///< 总流量
@property (nonatomic, assign) CGFloat bandwidth;                        ///< 1s内总带宽
@property (nonatomic, assign) CGFloat currentBandwidth;                 ///< 上次的带宽

@property (nonatomic, assign) NSInteger dropFrame;                      ///< 丢掉的帧数
@property (nonatomic, assign) NSInteger totalFrame;                     ///< 总帧数

@property (nonatomic, assign) NSInteger capturedAudioCount;             ///< 1s内音频捕获个数
@property (nonatomic, assign) NSInteger capturedVideoCount;             ///< 1s内视频捕获个数
@property (nonatomic, assign) NSInteger currentCapturedAudioCount;      ///< 上次的音频捕获个数
@property (nonatomic, assign) NSInteger currentCapturedVideoCount;      ///< 上次的视频捕获个数

@property (nonatomic, assign) NSInteger unSendCount;                    ///< 未发送个数（代表当前缓冲区等待发送的）

@property (nonatomic, assign) CGFloat cpu;                                ///< cpu占用率
@property (nonatomic, assign) CGFloat fps;                                ///< 屏幕刷新率
@property (nonatomic, assign) CGFloat memory;                             ///< 内存占用率
@property (nonatomic, assign) CGFloat uploadBitrate;                      ///< 上传码率

@property (nonatomic, assign) int vSetFPS;                               ///< 设置的视频帧率
@property (nonatomic, assign) int vCurrentFPS;                           ///< 当前视频帧率
@property (nonatomic, assign) int vgop;                                  ///< 视频gop
@property (nonatomic, assign) int vSetBitrate;                           ///< 设置的视频码率
@property (nonatomic, assign) int vCurrentBitrate;                       ///< 当前视频码率
@property (nonatomic, assign) int videoWidth;                        ///< 视频宽度
@property (nonatomic, assign) int videoHeight;                       ///< 视频高度
@property (nonatomic, assign) int aCurrentBitrate;                   ///< 当前音频码率
@property (nonatomic, strong) NSString *pushUrl;                     ///< 推流地址
@property (nonatomic, assign) float caches;                            ///< 缓冲大小
/*
 private static final int MSG_RTMP_CONNECTING = 0;（正在连接）
 private static final int MSG_RTMP_CONNECTED = 1;（已经连接）
 private static final int MSG_RTMP_VIDEO_STREAMING = 2;（正在发送视频流） 
 private static final int MSG_RTMP_AUDIO_STREAMING = 3; （正在发送音频流）
 private static final int MSG_RTMP_STOPPED = 4; （停止发送） 
 private static final int MSG_RTMP_DISCONNECTED = 5; （断开连接） 
 private static final int MSG_RTMP_SOCKET_EXCEPTION = 9; （SOCKET异常） 
 private static final int MSG_RTMP_IO_EXCEPTION = 10; （IO异常） 
 private static final int MSG_RTMP_ILLEGAL_ARGUMENT_EXCEPTION = 11; （非法参数异常） 
 private static final int MSG_RTMP_ILLEGAL_STATE_EXCEPTION = 12; （非法状态异常）*/
@property (nonatomic, assign) int pushEvent;

@property (nonatomic, assign) CGFloat temporaryVideoDataLeng;            ///< 临时数据长度用来计算当前码率
@property (nonatomic, assign) CGFloat temporaryVideoCount;               ///< 临时数据个数 (当改值为80的时候, 取得时间差 和临时数据长度来相除计算当前码率)
@property (nonatomic, assign) long long temporaryVideoTime;                ///< 临时时间

@property (nonatomic, assign) CGFloat temporaryAudioDataLeng;            ///< 临时数据长度用来计算当前码率
@property (nonatomic, assign) CGFloat temporaryAudioCount;               ///< 临时数据个数 (当改值为80的时候, 取得时间差 和临时数据长度来相除计算当前码率)
@property (nonatomic, assign) long long temporaryAudioTime;                ///< 临时时间

@property (nonatomic, assign) int cachesNumber;
@property (nonatomic, assign) int cachesSum;


@end
