//Copyright 2018 Huawei Technologies Co.,Ltd.

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import "HWLiveAudioConfiguration.h"
#import "HWLiveVideoConfiguration.h"
#import "HWLiveDebug.h"
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>


@protocol HWLiveViewDelegate <NSObject>
@optional
/** live status changed will callback */
- (void)liveStatusCallback:(HWLiveState)state;
/** live debug info callback */
- (void)liveDebugInfoCallback:(nullable HWLiveDebug *)debugInfo;
/** callback socket errorcode */
- (void)liveErrorCodeCallback:(HWLiveSocketErrorCode)errorCode;
@end

//@class HWLiveSession;
@interface HWLivePusherView : UIView
/// 代理
@property (nullable, nonatomic, weak) id<HWLiveViewDelegate> delegateLiveView;

/* 聚焦模式 */
@property (nonatomic, assign) AVCaptureFocusMode focusMode;

/* 焦点 */
@property (nonatomic, assign) CGPoint focusPoint;
 
/* 焦距  */
@property (nonatomic, assign) CGFloat videoZoomFactor;

/* 曝光点 */
@property (nonatomic, assign) CGPoint exposurePoint;

/* 曝光模式 */
@property (nonatomic, assign) AVCaptureExposureMode exposureModel;

/// 最大缩放值
@property (nonatomic, assign, readonly) CGFloat maxAvailableVideoZoomFactor;
/// 最小缩放值
@property (nonatomic, assign, readonly) CGFloat minAvailableVideoZoomFactor;

/// 音视频配置
- (instancetype _Nullable )initWithFrame:(CGRect)frame
           audioConfiguration:(nullable HWLiveAudioConfiguration *)audioConfiguration
           videoConfiguration:(nullable HWLiveVideoConfiguration *)videoConfiguration
                  captureType:(HWLiveCaptureTypeMask)captureType;

///开启预览
- (void)startPreviewView;

///关闭预览
- (void)stopPreviewView;

/// 开始推流
- (void)startPush:(NSString*_Nullable)rtmpUrl;

/// 停止推流
- (void)stopPush;

/// 是否正在推流
- (BOOL)isPushing;

/// 切换摄像头
- (void)switchCamera:(AVCaptureDevicePosition)captureDevicePosition;

/// 当前摄像头位置
- (AVCaptureDevicePosition)getCameraDevicePosition;

/// 开关闪光灯
- (void)toggleFlashLight:(BOOL)isFlashOn;

/// 闪光灯是否开启
- (BOOL)isFlashLightOn;

/// 设置镜像
- (void)setMirror:(BOOL)isMirror;

/// 设置水印
- (void)setWaterMark:(UIView *_Nullable)waterMarkView;

/// 设置美颜开启
- (void)setBeautyOn:(BOOL)isBeautyOn;


/// 设置数据上报是否开启. 默认是开启的.如果需要关闭设置为NO即可
/// @param isEnable BOOL YES 开启数据上报, NO关闭数据上报
- (void)setStatisticEnable:(BOOL)isEnable;

/// 设置美颜级别
/* Default is 0.5, between 0.0 ~ 1.0 */
- (void)setBeautyLevel:(CGFloat)beautyLevel brightLevel:(CGFloat)brightLevel;

- (void)setAudioOnly:(BOOL)isAudioOnly;
- (void)setVideoOnly:(BOOL)isVideoOnly;

/// 开关调试信息
-(void)turnOnDebugInfo:(BOOL)isTurnOn;

/** support outer input yuv or rgb video(set LFLiveCaptureTypeMask) .*/
- (void)pushVideo:(nullable CVPixelBufferRef)pixelBuffer;

/** support outer input pcm audio(set LFLiveCaptureTypeMask) .*/
- (void)pushAudio:(nullable NSData*)audioData;

/**
 <#Description#>
 
 @param audioSampleBuffer PCM
 */
- (void)pushAudioSampleBuffer:(nullable CMSampleBufferRef)audioSampleBuffer;

- (void)destroy;
@end
